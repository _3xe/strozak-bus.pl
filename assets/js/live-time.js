 function liveTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('live').innerHTML =
    h + ":" + m + ":" + s;
    setTimeout(function(){ liveTime()}, 1000);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};
    return i;
}

function liveDay() {
    var day = new Date();
    var d = day.getDay();
    if (d == 0) {
        document.getElementById('day').innerHTML = 
        "niedziela, dziś nie kursujemy!";
    }
    if (d == 1) {
        document.getElementById('day').innerHTML = 
        "poniedziałek";
    }
    if (d == 2) {
        document.getElementById('day').innerHTML = 
        "wtorek";
    }
    if (d == 3) {
        document.getElementById('day').innerHTML = 
        "środa";
    }
    if (d == 4) {
        document.getElementById('day').innerHTML = 
        "czwartek";
    }
    if (d == 5) {
        document.getElementById('day').innerHTML = 
        "piątek";
    }
    if (d == 6) {
        document.getElementById('day').innerHTML = 
        "sobota";
    }
}